﻿using Unity.Entities;
using Unity.Mathematics;

namespace Components
{
    /// <summary>
    /// Keeps the indexes of Jelly in the board
    /// </summary>
    public struct BoardIndex : IComponentData
    {
        public int2 Value;
    }
}