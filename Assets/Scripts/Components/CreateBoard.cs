﻿using Unity.Entities;

namespace Components
{
    /// <summary>
    /// Start game command input component.
    /// `Disposable Entity`
    /// </summary>
    public struct CreateBoard : IComponentData
    {
    }
}