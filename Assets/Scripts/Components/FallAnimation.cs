using Unity.Entities;

namespace Components
{
    public struct FallAnimation : IComponentData
    {
        public float Time;
        public float Duration;
        public int StartIndex;
        public int EndIndex;
    }
}