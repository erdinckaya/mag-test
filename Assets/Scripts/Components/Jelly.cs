﻿using Unity.Entities;
using Util;

namespace Components
{
    /// <summary>
    /// Tag component keeps the type of Jelly
    /// </summary>
    public struct Jelly : IComponentData
    {
        public JellyType Type;
        public Entity Face;
    }
}