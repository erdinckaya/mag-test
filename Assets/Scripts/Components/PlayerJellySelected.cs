﻿using Unity.Entities;

namespace Components
{
    public struct PlayerJellySelected : IComponentData
    {
        public Entity Owner;
    }
}