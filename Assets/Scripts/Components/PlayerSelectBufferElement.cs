﻿using Unity.Entities;

namespace Components
{
    public struct PlayerSelectBufferElement : IBufferElementData
    {
        public Entity Value;
    }
}