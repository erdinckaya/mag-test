using Unity.Entities;

namespace Components
{
    public struct Score : IComponentData
    {
        public int Value;
    }
}