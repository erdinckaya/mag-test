using Unity.Entities;

namespace Components
{
    public struct ShrinkAnimation : IComponentData
    {
        public float Time;
        public float Duration;
        public float StartScale;
        public float EndScale;
    }
}