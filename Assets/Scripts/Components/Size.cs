﻿using Unity.Entities;
using Unity.Mathematics;

namespace Components
{
    /// <summary>
    /// Keeps the actual size of Jelly.
    /// </summary>
    public struct Size : IComponentData
    {
        public float2 Value;
    }
}