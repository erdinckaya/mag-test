﻿using Components;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using Util;

// Static entity creator class.
public static class EntityFactory
{
    public static void CreatePlayerJellySelectedEntity(EntityManager entityManager, Entity owner)
    {
        var entities  = new NativeArray<Entity>(1, Allocator.Temp);
        var archetype = entityManager.CreateArchetype(typeof(PlayerJellySelected));
        entityManager.CreateEntity(archetype, entities);
        foreach (var entity in entities)
        {
            entityManager.SetComponentData(entity, new PlayerJellySelected {Owner = owner});
        }

        entities.Dispose();
    }

    public static void CreatePlayer(EntityManager entityManager)
    {
        var entities = new NativeArray<Entity>(1, Allocator.Temp);
        var archetype = entityManager.CreateArchetype(
            typeof(Player),
            typeof(PlayerSelectBufferElement)
        );
        entityManager.CreateEntity(archetype, entities);
        entities.Dispose();
    }

    public static void CreateFace(EntityManager entityManager, float2 pos, ref Jelly jelly)
    {
        var faceAsset = Config.Instance.jellyConfig.faces[(int) jelly.Type];
        var entities = new NativeArray<Entity>(1, Allocator.Temp);
        var archetype = entityManager.CreateArchetype(
            typeof(LocalToWorld),
            typeof(Face),
            typeof(Translation),
            typeof(Scale),
            typeof(RenderMesh)
        );
        entityManager.CreateEntity(archetype, entities);
        pos -= new float2(0, Config.Instance.jellyConfig.faceDelta);
        foreach (var entity in entities)
        {
            entityManager.SetComponentData(entity, new Translation {Value = new float3(pos.x, pos.y, 5)});
            entityManager.SetComponentData(entity, new Scale {Value = Config.Instance.jellyConfig.scale});
            var material = faceAsset.material;
            material.renderQueue = 3001;
            entityManager.SetSharedComponentData(entity, new RenderMesh
            {
                mesh     = faceAsset.ToMesh(),
                material = material
            });
            jelly.Face = entity;
        }

        entities.Dispose();
    }

    public static void CreateJelly(EntityManager entityManager, JellyType jellyType, int2 boardIndex)
    {
        // Calculate the position
        var start     = Config.Instance.boardConfig.leftTopOfBoard;
        var gap       = Config.Instance.boardConfig.gapBetweenJellies;
        var jellySize = Config.Instance.jellyConfig.jellies[0].sprite.bounds.size * Config.Instance.jellyConfig.scale;
        var pos       = start + new float2((jellySize.x + gap) * boardIndex.x, (jellySize.y + gap) * -boardIndex.y);

        var jellyAsset = Config.Instance.jellyConfig.jellies[(int) jellyType];
        var size       = jellyAsset.sprite.bounds.size;
        size *= Config.Instance.jellyConfig.scale;
        

        var entities = new NativeArray<Entity>(1, Allocator.Temp);
        var jellyArchetype = entityManager.CreateArchetype(typeof(LocalToWorld),
            typeof(Translation),
            typeof(Scale),
            typeof(RenderMesh),
            typeof(Size),
            typeof(Jelly),
            typeof(BoardIndex)
        );
        entityManager.CreateEntity(jellyArchetype, entities);
        foreach (var entity in entities)
        {
            entityManager.SetComponentData(entity, new Jelly {Type        = jellyType, Face = Entity.Null});
            entityManager.SetComponentData(entity, new Translation {Value = new float3(pos.x, pos.y, 0)});
            entityManager.SetComponentData(entity, new Scale {Value       = Config.Instance.jellyConfig.scale});
            entityManager.SetComponentData(entity, new Size {Value        = new float2(size.x, size.y)});
            entityManager.SetComponentData(entity, new BoardIndex {Value  = boardIndex});
            entityManager.SetSharedComponentData(entity, new RenderMesh
            {
                mesh     = jellyAsset.ToMesh(),
                material = jellyAsset.material
            });
        }
        
        entities.Dispose();
        
    }

    public static void CreateInputEntity<T>(EntityManager entityManager)
    {
        var entities  = new NativeArray<Entity>(1, Allocator.Temp);
        var archetype = entityManager.CreateArchetype(typeof(T));
        entityManager.CreateEntity(archetype, entities);
        entities.Dispose();
    }
    
    public static void CreateScoreEntity(EntityManager entityManager, int score)
    {
        var entities  = new NativeArray<Entity>(1, Allocator.Temp);
        var archetype = entityManager.CreateArchetype(typeof(Score));
        entityManager.CreateEntity(archetype, entities);
        foreach (var entity in entities)
        {
            entityManager.SetComponentData(entity, new Score {Value = score});
        }
        entities.Dispose();
    }

    public static void DestroyJelly(EntityCommandBuffer postUpdateCommands, EntityManager entityManager, Entity jelly)
    {
        var jellyComp = entityManager.GetComponentData<Jelly>(jelly);
        if (jellyComp.Face != Entity.Null)
        {
            postUpdateCommands.DestroyEntity(jellyComp.Face);
        }
        postUpdateCommands.DestroyEntity(jelly);
    }
}