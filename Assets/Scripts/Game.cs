﻿using UI;
using Unity.Mathematics;
using UnityEngine;
using Util;

public class Game
{
    private static Game _instance;
    public static Game Instance => _instance ?? (_instance = new Game());

    private Game()
    {
    }

    private int _score;

    public int Score
    {
        get => _score;
        set
        {
            if (value != _score)
            {
                _score = value;
                UIManager.Instance.UpdateScore(_score, TotalScore);
            }
        }
    }

    private int _level = 1;

    public int Level
    {
        get => _level;
        set
        {
            if (value != _level)
            {
                _level = value;
                UIManager.Instance.UpdateLevel(_level);
            }
        }
    }

    private const long ScoreMultiplier = 20;
    private long _firstTotalScore;
    private long _secondTotalScore;

    // Calculates next level score.
    private long Fibonacchi(int n)
    {
        double phi = (1 + math.sqrt(5)) / 2;
        return (long) math.round(math.pow(phi, n) / math.sqrt(5));
    }

    public long TotalScore => Fibonacchi(Level) * ScoreMultiplier;

    public bool InputAllowed = true;
}