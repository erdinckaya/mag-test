﻿using Components;
using Unity.Entities;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    // Starts game.
    private void Start()
    {
        EntityFactory.CreatePlayer(World.DefaultGameObjectInjectionWorld.EntityManager);
        EntityFactory.CreateInputEntity<CreateBoard>(World.DefaultGameObjectInjectionWorld.EntityManager);
    }
}