using Components;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace Systems
{
    public class AddFaceSystem : ComponentSystem
    {
        // Checks buffer and adds faces.
        protected override void OnUpdate()
        {
            Entities.WithAll<Player>().ForEach((Entity entity, ref Player player) =>
            {
                // Get Buffer.
                var selectedJellies = EntityManager.GetBuffer<PlayerSelectBufferElement>(entity);
                // Create jelly entity array and pass jellies to animation
                var jellies = new Entity[selectedJellies.Length];
                for (var index = 0; index < selectedJellies.Length; index++)
                {
                    var element = selectedJellies[index];
                    jellies[index] = element.Value;
                }
                
                // Add faces if it does not already.
                Entities.WithAll<Jelly>().ForEach((Entity e, ref Jelly jelly, ref Translation translation) =>
                {
                    foreach (var jellyEntity in jellies)
                    {
                        if (jellyEntity == e)
                        {
                            if (jelly.Face == Entity.Null)
                            {
                                var pos = new float2(translation.Value.x, translation.Value.y);
                                EntityFactory.CreateFace(EntityManager, pos, ref jelly);
                            }
                            break;
                        }
                    }
                });
            });
            
            
        }
    }
}