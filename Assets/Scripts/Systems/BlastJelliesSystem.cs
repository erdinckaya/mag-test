﻿using System.Linq;
using Components;
using Unity.Entities;
using Unity.Transforms;
using Util;

namespace Systems
{
    public class BlastJelliesSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            Entities.WithAll<BlastJellies>().ForEach((Entity entity, ref BlastJellies blastJellies) =>
            {
                Game.Instance.InputAllowed = false;

                Entities.WithAll<Player>().ForEach((Entity e, ref Player player) =>
                {
                    var selectedJellies = EntityManager.GetBuffer<PlayerSelectBufferElement>(e);
                    // Check for match, if it is not bigger or equal to ${Config.Instance.leastMatchCount} than return false. 
                    if (selectedJellies.Length < Config.Instance.leastMatchCount)
                    {
                        selectedJellies.Clear();
                        EntityFactory.CreateInputEntity<EndOfUserInput>(EntityManager);
                        return;
                    }

                    // Create jelly entity array and pass jellies to animation
                    var jellies = new Entity[selectedJellies.Length];
                    for (var index = 0; index < selectedJellies.Length; index++)
                    {
                        var element = selectedJellies[index];
                        jellies[index] = element.Value;
                    }

                    // Start the animation
                    StartBlastAnimation(jellies);
                });

                PostUpdateCommands.DestroyEntity(entity);
            });
        }

        private void StartBlastAnimation(Entity[] jellies)
        {
            // Start animations
            var duration = Config.Instance.jellyConfig.shrinkAnimationDuration;
            foreach (var jelly in jellies)
            {
                // Start shrink animation.
                var scale = EntityManager.GetComponentData<Scale>(jelly);
                var jellyComp = EntityManager.GetComponentData<Jelly>(jelly);
                PostUpdateCommands.AddComponent(jelly, new ShrinkAnimation
                {
                    Duration   = duration,
                    StartScale = scale.Value,
                    EndScale   = 0
                });
                
                // Shrink faces as well.
                if (jellyComp.Face != Entity.Null)
                {
                    var faceScale = EntityManager.GetComponentData<Scale>(jellyComp.Face);
                    PostUpdateCommands.AddComponent(jellyComp.Face, new ShrinkAnimation
                    {
                        Duration   = duration,
                        StartScale = faceScale.Value,
                        EndScale   = 0
                    });
                }
            }
        }
    }
}