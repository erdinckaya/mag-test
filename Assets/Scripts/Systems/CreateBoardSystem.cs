﻿using Components;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;
using Util;
using Random = UnityEngine.Random;

namespace Systems
{
    public class CreateBoardSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            Entities.WithAll<CreateBoard>().ForEach((Entity entity, ref CreateBoard createBoard) =>
            {
                PrepareBoard(EntityManager);
                PostUpdateCommands.DestroyEntity(entity);
            });
        }

        private static void PrepareBoard(EntityManager entityManager)
        {
            // Create board.
            CreateBoard(entityManager);
            // Enable the user input.
            Game.Instance.InputAllowed = true;
        }

        private static void CreateBoard(EntityManager entityManager)
        {
            // Calculate the starting point.
            var gap = Config.Instance.boardConfig.gapBetweenJellies;
            var jellyAsset = Config.Instance.jellyConfig.jellies[0];
            var jellySize = jellyAsset.sprite.bounds.size * Config.Instance.jellyConfig.scale;
            var boardWidth = gap * (Config.Instance.boardConfig.column - 1) +
                             jellySize.x * Config.Instance.boardConfig.column;
            var boardHeight = gap * (Config.Instance.boardConfig.row - 1) +
                              jellySize.y * Config.Instance.boardConfig.row;

            var startX = -Config.Instance.boardConfig.screenWidth * 0.5f + (Config.Instance.boardConfig.screenWidth - boardWidth) * 0.5f;
            var startY = Config.Instance.boardConfig.screenHeight * 0.5f - (Config.Instance.boardConfig.screenHeight - boardHeight) * 0.5f;
            var start = new float2(startX, startY) + new float2(jellySize.x, -jellySize.y) * 0.5f;
            // Store our let top position, we are gonna use it in future.
            Config.Instance.boardConfig.leftTopOfBoard = start;
            for (int col = 0; col < Config.Instance.boardConfig.column; col++)
            {
                for (int row = 0; row < Config.Instance.boardConfig.row; row++)
                {
                    var boardIndex = new int2(col, row);
                    var jellyType = (JellyType) Random.Range(0, (int) JellyType.Count);
                    EntityFactory.CreateJelly(entityManager, jellyType, boardIndex);
                }
            }

            // Adjust camera for different sizes.
            if (Config.Instance.boardConfig.column > 6 || Config.Instance.boardConfig.row > 6)
            {
                Camera.main.orthographicSize = 9;
            }
        }
    }
}