using Components;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;
using Util;
using Random = UnityEngine.Random;

namespace Systems
{
    public class CreateNewJelliesSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            Entities.WithAll<CreateNewJellies>().ForEach((Entity entity, ref CreateNewJellies createNewJellies) =>
            {
                Entities.WithAll<Player>().ForEach((Entity e, ref Player player) =>
                {
                    // If match count does not satisfy return.
                    var selectedJellies = EntityManager.GetBuffer<PlayerSelectBufferElement>(e);
                    if (selectedJellies.Length < Config.Instance.leastMatchCount)
                    {
                        return;
                    }

                    // Create jelly entity array and pass jellies to animation
                    var jellies = new Entity[selectedJellies.Length];
                    for (var index = 0; index < selectedJellies.Length; index++)
                    {
                        var element = selectedJellies[index];
                        jellies[index] = element.Value;
                    }
                    // Clear buffer
                    selectedJellies.Clear();

                    // Columns of new jellies
                    var columns = new int[Config.Instance.boardConfig.column];

                    foreach (var jelly in jellies)
                    {
                        // Get entity board index.
                        var boardIndex = EntityManager.GetComponentData<BoardIndex>(jelly);
                        // Decrease the new board index of created jelly. 
                        columns[boardIndex.Value.x]--;

                        // Create new jelly.
                        var jellyType    = (JellyType) Random.Range(0, (int) JellyType.Count);
                        var newBoarIndex = new int2(boardIndex.Value.x, columns[boardIndex.Value.x]);
                        EntityFactory.CreateJelly(EntityManager, jellyType, newBoarIndex);
                        // Mark jelly for destroy
                        EntityManager.AddComponentData(jelly, new DestroyJellies());
                    }
                    
                    EntityFactory.CreateScoreEntity(EntityManager, jellies.Length);
                });

                PostUpdateCommands.DestroyEntity(entity);
            });
        }
    }
}