using System.Collections.Generic;
using Components;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;
using Util;

namespace Systems
{
    public class DestroyJellySystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            var jellies = new List<int2>();
            Entities.WithAll<DestroyJellies>().ForEach((Entity entity, ref DestroyJellies destroyJellies, ref BoardIndex boardIndex) =>
            {
                // Store the blasted jellies' indexes to determine the fall animations' destinations.
                jellies.Add(boardIndex.Value);

                // Destroy jelly with face.
                EntityFactory.DestroyJelly(PostUpdateCommands, EntityManager, entity);
            });

            // Start fall animation.
            CreateFallAnimations(jellies);
        }

        private void CreateFallAnimations(IReadOnlyCollection<int2> jellies)
        {
            Entities.WithAll<Jelly>().ForEach((Entity entity, ref BoardIndex boardIndex) =>
            {
                foreach (var index in jellies)
                {
                    // Check jellies whose have index that is above of blasted ones.
                    if (index.x == boardIndex.Value.x && index.y > boardIndex.Value.y)
                    {
                        // If it has already animation increase the destination index.
                        var hasFallAnimation = EntityManager.HasComponent<FallAnimation>(entity);
                        if (hasFallAnimation)
                        {
                            var fallAnimation = EntityManager.GetComponentData<FallAnimation>(entity);
                            fallAnimation.EndIndex++;
                            EntityManager.SetComponentData(entity, fallAnimation);
                        }
                        else
                        {
                            // Add fall animation.
                            EntityManager.AddComponentData(entity, new FallAnimation
                            {
                                Duration   = Config.Instance.boardConfig.jellyFallDuration,
                                StartIndex = boardIndex.Value.y,
                                EndIndex   = boardIndex.Value.y + 1
                            });
                        }
                    }
                }
            });
        }
    }
}