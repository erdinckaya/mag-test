using Components;
using Unity.Entities;

namespace Systems
{
    public class EndOfUserInputSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            // If all animation cycles are done this component is added into system.
            Entities.WithAll<EndOfUserInput>().ForEach((Entity entity, ref EndOfUserInput removeFace) =>
            {
                // Remove faces.
                RemoveFaces();
                // Destroy the input entity.
                PostUpdateCommands.DestroyEntity(entity);
                // Enable the user input.
                Game.Instance.InputAllowed = true;
            });
        }

        private void RemoveFaces()
        {
            // Check all jellies and remove their faces.
            Entities.WithAll<Jelly>().ForEach((Entity e, ref Jelly jelly) =>
            {
                if (jelly.Face != Entity.Null)
                {
                    // Destroy the face entity.
                    PostUpdateCommands.DestroyEntity(jelly.Face);
                    jelly.Face = Entity.Null;
                }
            });
        }
    }
}