using Components;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using Util;

namespace Systems
{
    public class FallAnimationSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            var deltaTime = Time.DeltaTime;
            Entities.WithAll<FallAnimation>().ForEach(
                (Entity entity, ref FallAnimation animation, ref Translation translation, ref BoardIndex boardIndex) =>
                {
                    if (animation.Time <= animation.Duration)
                    {
                        // Calculate the interpolated position.
                        var ratio  = animation.Time / animation.Duration;
                        var startPos = CalculateIndexPos(new int2(boardIndex.Value.x, animation.StartIndex));
                        var endPos = CalculateIndexPos(new int2(boardIndex.Value.x, animation.EndIndex));
                        var newPos = (endPos.y - startPos.y) * ratio;
                        
                        translation.Value = new float3(startPos.x, startPos.y + newPos, 0);

                        animation.Time += deltaTime;
                    }
                    else // Remove Fall animation.
                    {
                        boardIndex.Value.y = animation.EndIndex;
                        EntityFactory.CreateInputEntity<EndOfUserInput>(EntityManager);
                        PostUpdateCommands.RemoveComponent<FallAnimation>(entity);
                    }
                });
        }

        /// <summary>
        /// Calculates the board world position with given board index.
        /// </summary>
        /// <param name="boardIndex"></param>
        /// <returns></returns>
        private static float2 CalculateIndexPos(int2 boardIndex)
        {
            var start     = Config.Instance.boardConfig.leftTopOfBoard;
            var gap       = Config.Instance.boardConfig.gapBetweenJellies;
            var jellySize = Config.Instance.jellyConfig.jellies[0].sprite.bounds.size * Config.Instance.jellyConfig.scale;
            return start + new float2((jellySize.x + gap) * boardIndex.x, (jellySize.y + gap) * -boardIndex.y);
        }
    }
}