﻿using Components;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace Systems
{
    /// <summary>
    /// Handles player's inputs. 
    /// </summary>
    public class PlayerInputSystem : ComponentSystem
    {
        /// <summary>
        /// Enum for mouse keys. 
        /// </summary>
        private enum MouseKey
        {
            Left,
            Right,
            Middle
        }

        private int2 _lastIndex = new int2(-1, -1);

        protected override void OnUpdate()
        {
            // Check player can act or not
            if (!Game.Instance.InputAllowed)
            {
                return;
            }

            // Traverse all jellies and check mouse is inside of that jelly.
            if (Input.GetMouseButton((int) MouseKey.Left))
            {
                var mouse = Input.mousePosition;
                mouse.z = 10.0f;
                mouse = Camera.main.ScreenToWorldPoint(mouse);
                
                Entities.WithAll<Jelly>().ForEach((Entity entity, ref Size size, ref Jelly jelly, ref Translation translation, ref BoardIndex boardIndex) =>
                {
                    var center = new float2(translation.Value.x, translation.Value.y);
                    var point = new float2(mouse.x, mouse.y);
                    var isPointInside = IsPointInside(point, center, size.Value);

                    // Check user input whether inside of jelly or not.
                    if (isPointInside)
                    {
                        if (_lastIndex.x != boardIndex.Value.x || _lastIndex.y != boardIndex.Value.y)
                        {
                            // Start the selection.
                            EntityFactory.CreatePlayerJellySelectedEntity(EntityManager, entity);
                            _lastIndex = boardIndex.Value;
                        }
                    }
                });
            }
            
            // If mouse or finger up go to blast jelly state.
            if (Input.GetMouseButtonUp((int) MouseKey.Left))
            {
                _lastIndex = new int2(-1, -1);
                EntityFactory.CreateInputEntity<BlastJellies>(EntityManager);
            }
        }

        /// <summary>
        /// Basic point inside of rectangle calculation.
        /// </summary>
        /// <param name="point"></param>
        /// <param name="center"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        private static bool IsPointInside(float2 point, float2 center, float2 size)
        {
            var topLeft = center + new float2(-size.x, +size.y) * 0.5f;
            var rightBottom = center + new float2(size.x, -size.y) * 0.5f;

            return point.x >= topLeft.x && point.x <= rightBottom.x
                                        && point.y <= topLeft.y && point.y >= rightBottom.y;
        }
    }
}