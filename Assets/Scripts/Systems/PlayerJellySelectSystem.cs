﻿using Components;
using Unity.Entities;
using Unity.Mathematics;

namespace Systems
{
    public class PlayerJellySelectSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            Entities.WithAll<PlayerJellySelected>().ForEach((Entity entity, ref PlayerJellySelected playerJellySelected) =>
            {
                var selected = playerJellySelected;
                // Since Unity DOTS does not have any singleton entity mechanism we need to fetch our singleton entities like this.
                Entities.WithAll<Player>().ForEach((Entity e, ref Player player) =>
                {
                    var selectedJellies = EntityManager.GetBuffer<PlayerSelectBufferElement>(e);
                    var canSelectJelly = CanSelectJelly(ref selectedJellies, selected.Owner, out var blastJellies, out var popJelly);

                    // Player chose the wrong color or not neighbour so blast them
                    // and terminate the selection.
                    if (blastJellies)
                    {
                        EntityFactory.CreateInputEntity<BlastJellies>(EntityManager);
                        return;
                    }
                    
                    // If we have that jelly before pop jelly from buffer
                    // and remove face.
                    if (popJelly)
                    {
                        PopJelly(selectedJellies);
                    }


                    // Add new jelly
                    if (canSelectJelly)
                    {
                        selectedJellies.Add(new PlayerSelectBufferElement {Value = selected.Owner});
                    }
                });
                PostUpdateCommands.DestroyEntity(entity);
            });
        }

        /// <summary>
        /// Pops jelly from selected jelly buffer.
        /// </summary>
        /// <param name="selectedJellies"></param>
        private void PopJelly(DynamicBuffer<PlayerSelectBufferElement> selectedJellies)
        {
            // Get last jelly in buffer.
            var jellEntity = selectedJellies[selectedJellies.Length - 1].Value;

            var jelly = EntityManager.GetComponentData<Jelly>(jellEntity);
            var jType = jelly.Type;
            // Destroy the face.
            PostUpdateCommands.DestroyEntity(jelly.Face);
            // Update the component.
            EntityManager.SetComponentData(jellEntity, new Jelly {Type = jType, Face = Entity.Null});

            // Remove from buffer.
            selectedJellies.RemoveAt(selectedJellies.Length - 1);
        }

        private bool CanSelectJelly(ref DynamicBuffer<PlayerSelectBufferElement> selectedJellies, Entity candidateJelly, out bool blastJellies, out bool popJelly)
        {
            popJelly = false;
            blastJellies = false;
            // Check Buffer is empty, if it is add jelly. 
            if (selectedJellies.Length == 0)
            {
                return true;
            }
            
            // Get jelly type. We assume that all jellies are same type 
            // so that we can check first one safely.
            var jellyType = EntityManager.GetComponentData<Jelly>(selectedJellies[0].Value).Type;
            var candidateJellyType = EntityManager.GetComponentData<Jelly>(candidateJelly).Type;
            if (jellyType != candidateJellyType)
            {
                blastJellies = true;
                return false;
            }

            // Check the indexes to decide they are adjacent or not.
            var lastIndex = EntityManager.GetComponentData<BoardIndex>(selectedJellies[selectedJellies.Length - 1].Value).Value;
            var candidateJellyIndex = EntityManager.GetComponentData<BoardIndex>(candidateJelly).Value;
            var diff = math.abs(lastIndex - candidateJellyIndex);
            // This is itself not need to do anything
            if (diff.x == 0 && diff.y == 0)
            {
                return false;
            }

            // Check for neighbour also diagonal.
            var isNeighbour = diff.x <= 1 && diff.y <= 1;
            if (!isNeighbour)
            {
                blastJellies = true;
                return false;
            }

            // Check it has already in the buffer.
            foreach (var element in selectedJellies)
            {
                var selectedIndex = EntityManager.GetComponentData<BoardIndex>(element.Value).Value;
                if (candidateJellyIndex.x == selectedIndex.x && candidateJellyIndex.y == selectedIndex.y)
                {
                    popJelly = true;
                    return false;
                }
            }

            // Apply de Morgan, if there is not wrong thing in your life, you are living right.
            return true;
        }
    }
}