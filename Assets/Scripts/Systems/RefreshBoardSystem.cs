using Components;
using Unity.Entities;

namespace Systems
{
    public class RefreshBoardSystem : ComponentSystem
    {
        /// <summary>
        /// Refreshes the board. Destroys everything and recreates them all.
        /// </summary>
        protected override void OnUpdate()
        {
            Entities.WithAll<RefreshBoard>().ForEach((Entity entity, ref RefreshBoard refreshBoard) =>
            {
                ClearBuffer();
                DestroyJellies();
                EntityFactory.CreateInputEntity<CreateBoard>(EntityManager);
                PostUpdateCommands.DestroyEntity(entity);
            });
        }

        /// <summary>
        /// Destroy jellies with faces.
        /// </summary>
        private void DestroyJellies()
        {
            Entities.WithAll<Jelly>().ForEach((Entity entity, ref Jelly jelly) => { EntityFactory.DestroyJelly(PostUpdateCommands, EntityManager, entity); });
        }

        /// <summary>
        /// Clears the selected jellies buffer.
        /// </summary>
        private void ClearBuffer()
        {
            Entities.WithAll<Player>().ForEach((Entity e, ref Player player) =>
            {
                var selectedJellies = EntityManager.GetBuffer<PlayerSelectBufferElement>(e);
                selectedJellies.Clear();
            });
        }
    }
}