using Components;
using Unity.Entities;

namespace Systems
{
    public class ScoreSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            // Listens score input and updates the user interface, and also checks the 
            // next level.
            Entities.WithAll<Score>().ForEach((Entity entity, ref Score score) =>
            {
                if (Game.Instance.Score + score.Value >= Game.Instance.TotalScore)
                {
                    Game.Instance.Level++;
                    Game.Instance.Score = 0;
                    // Next level
                    EntityFactory.CreateInputEntity<RefreshBoard>(EntityManager);
                }
                else
                {
                    Game.Instance.Score += score.Value;
                }
                
                PostUpdateCommands.DestroyEntity(entity);
            });
        }
    }
}