using Components;
using Unity.Entities;
using Unity.Transforms;

namespace Systems
{
    public class ShrinkSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            var deltaTime = Time.DeltaTime;
            Entities.WithAll<ShrinkAnimation>().ForEach((Entity entity, ref ShrinkAnimation animation, ref Scale scale) =>
            {
                if (animation.Time <= animation.Duration)
                {
                    // Calculate the interpolated scale.
                    var ratio = 1.0f - animation.Time / animation.Duration;
                    var newScale = (animation.StartScale - animation.EndScale) * ratio;
                    scale.Value = newScale;
                    animation.Time += deltaTime;
                }
                else // Iterate to next animation.
                {
                    EntityFactory.CreateInputEntity<CreateNewJellies>(EntityManager);
                    PostUpdateCommands.RemoveComponent<ShrinkAnimation>(entity);
                }
            });
        }
    }
}