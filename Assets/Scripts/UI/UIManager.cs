﻿using System;
using TMPro;
using Unity.Entities;
using UnityEngine;
using UnityEngine.UI;
using Util;

namespace UI
{
    /// <summary>
    /// Main UI manager.
    /// </summary>
    public class UIManager : MonoBehaviour
    {
        public static UIManager Instance;

        public TextMeshProUGUI scoreText;
        public TextMeshProUGUI levelText;

        public TextMeshProUGUI progressText;
        public Slider progressSlider;

        private void Start()
        {
            // Init values.
            Instance = this;
        }

        /// <summary>
        /// Updates score text
        /// </summary>
        /// <param name="score"></param>
        /// <param name="totalScore"></param>
        public void UpdateScore(int score, long totalScore)
        {
            scoreText.text = $"Score: {score}";
            var ratio = (float) score / totalScore;
            var progress = (int) (ratio * 100);
            Debug.Log($"Score {score} total {totalScore}");
            progressText.text = $"%{progress}";
            progressSlider.value = ratio;
        }
        
        
        /// <summary>
        /// Updates level text
        /// </summary>
        /// <param name="level"></param>
        public void UpdateLevel(int level)
        {
            levelText.text = $"Level: {level}";
        }
    }
}