﻿using System;
using UnityEngine;

namespace Util
{
    [Serializable]
    public struct AssetData
    {
        public Sprite sprite;
        public Material material;

        public Mesh ToMesh()
        {
            Mesh mesh = new Mesh
            {
                vertices  = Array.ConvertAll(sprite.vertices, i => (Vector3) i),
                uv        = sprite.uv,
                triangles = Array.ConvertAll(sprite.triangles, i => (int) i)
            };
            return mesh;
        }
    }
}