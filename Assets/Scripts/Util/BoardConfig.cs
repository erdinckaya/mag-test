﻿using System;
using Unity.Mathematics;

namespace Util
{
    [Serializable]
    public class BoardConfig
    {
        public int column = 6;
        public int row = 6;
        public float gapBetweenJellies = 0.2f;

        public float screenWidth = 14.0f;
        public float screenHeight = 16.0f;

        public float2 leftTopOfBoard;

        public float jellyFallDuration = 1.0f;
    }
}