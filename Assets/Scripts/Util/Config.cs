﻿using UnityEngine;

namespace Util
{
    public class Config : MonoBehaviour
    {
        
        public static Config Instance;
        private void Awake()
        {
            Instance = this;
        }

        public int leastMatchCount = 3;
        
        [SerializeField]
        public BoardConfig boardConfig;
        
        [SerializeField]
        public LevelConfig levelConfig;

        [SerializeField] public JellyConfig jellyConfig;
    }
}
