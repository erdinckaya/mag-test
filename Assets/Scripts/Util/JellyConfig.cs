﻿using System;
using UnityEngine;

namespace Util
{
    [Serializable]
    public class JellyConfig
    {
        [SerializeField]
        public float shrinkAnimationDuration = 1f;
        [SerializeField]
        public float scale = 0.5f;
        public float faceDelta = 0.8f;
        
        [SerializeField]
        public AssetData[] jellies = new AssetData[(int) JellyType.Count];
        public AssetData[] faces = new AssetData[(int) JellyType.Count];
    }
}